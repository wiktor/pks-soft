use actix_web::{
    error, get, middleware::Logger, post, web, App, Error, HttpRequest, HttpResponse, HttpServer,
};
use clap::Parser;
use futures::StreamExt;
use openpgp::cert::prelude::*;
use openpgp::parse::Parse;
use sequoia_openpgp as openpgp;
use service_binding::Listener;
use std::sync::Mutex;

const MAX_SIZE: usize = 262_144; // max payload size is 256k

#[derive(Debug, Parser)]
struct Args {
    #[clap(short = 'H', long)]
    host: String,

    #[clap(short, long)]
    secring: std::path::PathBuf,
}

#[post("/{fpr}")]
async fn index(
    req: HttpRequest,
    params: web::Path<(String,)>,
    mut payload: web::Payload,
    certs: web::Data<Mutex<Vec<Cert>>>,
) -> Result<HttpResponse, Error> {
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    let (fpr,) = params.into_inner();
    let certs = certs.lock().unwrap();
    let certs = certs
        .iter()
        .flat_map(|cert| cert.keys().secret())
        .find(|key| key.fingerprint().to_hex() == fpr);
    if let Some(cert) = certs {
        let cert = cert.key().parts_as_secret().unwrap().clone();
        let is_password_ok = (body.is_empty() && cert.has_unencrypted_secret())
            || cert.decrypt_secret(&body.to_vec().into()).is_ok();
        if is_password_ok {
            let location = format!(
                "unix://{}/{}/sign/{}/",
                req.headers().get("host").unwrap().to_str().unwrap(),
                fpr,
                String::from_utf8_lossy(&body)
            );
            Ok(HttpResponse::Ok()
                .content_type("text/plain")
                .append_header(("Location", location))
                .append_header(("Accept-Post", "application/vnd.pks.digest.sha256, application/vnd.pks.digest.sha384, application/vnd.pks.digest.sha512"))
                .body(vec![]))
        } else {
            Ok(HttpResponse::Forbidden().into())
        }
    } else {
        Ok(HttpResponse::NotFound().into())
    }
}

#[get("{fpr}/public")]
async fn public(
    params: web::Path<(String,)>,
    certs: web::Data<Mutex<Vec<Cert>>>,
) -> Result<HttpResponse, Error> {
    let (fpr,) = params.into_inner();
    let certs = certs.lock().unwrap();
    let certs = certs
        .iter()
        .flat_map(|cert| cert.keys().secret())
        .find(|key| key.fingerprint().to_hex() == fpr);
    if let Some(cert) = certs {
        let cert = cert.key().parts_as_public();
        use openpgp::crypto::mpi::PublicKey;
        let (content_type, sig) = match cert.mpis() {
            PublicKey::RSA { n, .. } => {
                ("application/vnd.pks.public.rsa.modulus", n.value().to_vec())
            }
            PublicKey::ECDSA { q, .. } => (
                "application/vnd.pks.public.ec.compressed",
                q.value().to_vec(),
            ),
            PublicKey::EdDSA { q, .. } => {
                let mut out = vec![];
                out.extend(q.value());
                out.remove(0);

                ("application/vnd.pks.public.ed25519.compressed", out)
            }
            _ => panic!("Unsupported key type"),
        };
        Ok(HttpResponse::Ok().content_type(content_type).body(sig))
    } else {
        Ok(HttpResponse::NotFound().into())
    }
}

#[post("{fpr}/sign//")]
async fn sign(
    req: HttpRequest,
    params: web::Path<(String,)>,
    mut payload: web::Payload,
    certs: web::Data<Mutex<Vec<Cert>>>,
) -> Result<HttpResponse, Error> {
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    let (fpr,) = params.into_inner();
    let certs = certs.lock().unwrap();
    let certs = certs
        .iter()
        .flat_map(|cert| cert.keys().secret())
        .find(|key| key.fingerprint().to_hex() == fpr);
    if let Some(cert) = certs {
        let cert = cert.key().parts_as_secret().unwrap().clone();
        //if !body.is_empty() {
        //    cert = cert.decrypt_secret(&body.to_vec().into()).unwrap();
        //}
        let is_password_ok = cert.has_unencrypted_secret();
        if is_password_ok {
            use openpgp::crypto::mpi::Signature;
            use openpgp::crypto::Signer;
            use openpgp::types::HashAlgorithm;
            let mut cert = cert.into_keypair().unwrap();
            let hash = match req.headers().get("content-type").unwrap().to_str().unwrap() {
                "application/vnd.pks.digest.sha1" => HashAlgorithm::SHA1,
                "application/vnd.pks.digest.sha256" => HashAlgorithm::SHA1,
                "application/vnd.pks.digest.sha384" => HashAlgorithm::SHA384,
                "application/vnd.pks.digest.sha512" => HashAlgorithm::SHA512,
                // raw message to be digested by the crypto backend
                // this is most likely a message for ed25519+sha512
                // and the `hash` parameter will be ignored
                "application/octet-stream" => HashAlgorithm::SHA512,
                algo => panic!("Unsupported hashing algo: {}", algo),
            };
            eprintln!("==> {}, {}", hash, body.len());
            let (content_type, sig) = match cert.sign(hash, &body).unwrap() {
                Signature::RSA { s } => ("application/vnd.pks.signature.rsa", s.value().to_vec()),
                Signature::ECDSA { r, s } => {
                    let mut sig = vec![];
                    sig.extend(r.value());
                    sig.extend(s.value());
                    ("application/vnd.pks.signature.ecdsa.rs", sig)
                }
                Signature::EdDSA { r, s } => {
                    let mut sig = vec![];
                    sig.extend(r.value());
                    sig.extend(s.value());
                    ("application/vnd.pks.signature.eddsa.rs", sig)
                }
                _ => panic!("Unknown signature"),
            };
            Ok(HttpResponse::Ok().content_type(content_type).body(sig))
        } else {
            Ok(HttpResponse::Forbidden().into())
        }
    } else {
        Ok(HttpResponse::NotFound().into())
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let args = Args::parse();

    let certs = CertParser::from_file(args.secring)
        .unwrap()
        .flatten()
        .collect::<Vec<Cert>>();
    certs
        .iter()
        .flat_map(|cert| cert.keys().secret())
        .for_each(|key| {
            eprintln!(" Key {}", key.fingerprint());
        });

    let certs = web::Data::new(Mutex::new(certs));

    let server = HttpServer::new(move || {
        App::new()
            .service(index)
            .service(sign)
            .service(public)
            .app_data(certs.clone())
            .wrap(Logger::default())
    });

    let listener = args.host.parse().unwrap();

    match listener {
        Listener::Unix(listener) => server.listen_uds(listener),
        Listener::Tcp(listener) => server.listen(listener),
    }
    .unwrap()
    .run()
    .await
}
