#!/bin/bash

set -euxo pipefail

cat *.pgp > ../all-keys.pgp

/app/target/debug/pks-soft -H tcp://127.0.0.1:8081 --secring ../all-keys.pgp &
sleep 1

echo Running

echo Sample message > message.txt

for key in *.pgp; do
    sq key extract-cert "$key" > "${key%.pgp}.pub"
done

# mock the console so that it always produces empty ("") passwords
mv /dev/tty /dev/orig-tty
ln -s /dev/null /dev/tty

# Use only "pub" files to make sure that the interaction goes through
# the private key store

for key in *.pub; do
    echo "Sign/verify using $key"
    sq sign --private-key-store http://127.0.0.1:8081 --signer-key "$key" message.txt > message.pgp
    sq verify --signer-cert "$key" message.pgp
done
