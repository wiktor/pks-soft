# pks-soft

[Private Key Store][] backend that provides signing and decryption
capabilities using OpenPGP software keys via the Sequoia PGP library.

*WARNING*: This is very much work in progress. What currently works is signing using ed25519 keys.

```sh
gpg --export-secret-keys > secring.gpg
cargo run -- -H unix:///$XDG_RUNTIME_DIR/pks-soft.sock --secring secring.gpg
```

Now you can use the `pks-soft.sock` socket via other PKS project such as our [ssh agent][].

[ssh agent]: https://gitlab.com/sequoia-pgp/ssh-agent-pks
[Private Key Store]: https://gitlab.com/sequoia-pgp/pks
