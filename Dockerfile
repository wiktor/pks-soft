FROM rust

RUN apt update -y -qq && \
    apt install -y -qq --no-install-recommends clang make pkg-config nettle-dev libssl-dev capnproto

RUN cargo install --git https://gitlab.com/sequoia-pgp/sequoia/ sequoia-sq

COPY Cargo.toml Cargo.lock /app/
WORKDIR /app
RUN mkdir .cargo
RUN mkdir src
RUN touch src/main.rs
RUN cargo vendor > .cargo/config

RUN rm src/main.rs
COPY src /app/src
RUN cargo build

COPY tests /app/tests
WORKDIR /app/tests
RUN ./test.sh
